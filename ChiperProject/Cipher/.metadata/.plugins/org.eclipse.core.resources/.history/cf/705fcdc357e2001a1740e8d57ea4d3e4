package challenge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import challenge.dao.GroceryItemDao;
import challenge.dao.GroceryListDao;
import challenge.model.GroceryItem;
import challenge.model.GroceryList;

@RestController
@RequestMapping("/grocery-list/{list-id}/grocery-item")
@CrossOrigin(origins = "*")
public class GroceryItemController {

	private GroceryListDao listDao;
	private GroceryItemDao itemDao;

	@GetMapping("/")
	public List<GroceryItem> findAllItems(@PathVariable("list-id") int listId) {
		if(listId== -1)
			return itemDao.findAll();
		
		return listDao.findByGListId(listId).getItemList();
	}

	@PostMapping("/")
	public String createItem(@PathVariable("list-id") int listId, @RequestBody GroceryItem myItem) {

		//save the item into the DB
		itemDao.save(myItem);
		
		GroceryList tempList = listDao.findByGListId(listId);
		tempList.getItemList().add(myItem);

		listDao.save(tempList);

		return "Successful";
	}
	
	@PutMapping("/{item-id}")
	public String updateItem(@PathVariable("list-id") int listId, @PathVariable("item-id") int itemId,
			@RequestBody GroceryItem myItem) {

		
		GroceryList tempList = listDao.findByGListId(listId);
		List<GroceryItem> tempItemList = tempList.getItemList();
		
		for(GroceryItem temp: tempItemList) {
			if(temp.getgItemId() == itemId) {
				temp.setgItemName(myItem.getgItemName());
				temp.setgItemType(myItem.getgItemType());
			}
		}
		
		listDao.save(tempList);

		return "Successful";
	}
	
	@DeleteMapping("/{item-id}")
	public String deleteItem(@PathVariable("list-id") int listId, @PathVariable("item-id") int itemId) {
		
		
		GroceryList tempList = listDao.findByGListId(listId);
		List<GroceryItem> tempItemList = tempList.getItemList();
		
		//delete item from the list
		GroceryItem toBeDeleted = null;
		for(GroceryItem temp: tempItemList) {
			if(temp.getgItemId() == itemId) {
				toBeDeleted = temp;
			}
		}
		
		if(toBeDeleted != null)
			tempItemList.remove(toBeDeleted);
		
		listDao.save(tempList);
		
		//delete item from the table
		itemDao.deleteById(itemId);

		return "Successful";
	}

	////////////////////////// CONSTRUCTORS, GETTERS, & SETTERS

	public GroceryItemController() {
	}

	@Autowired
	public GroceryItemController(GroceryListDao listDao, GroceryItemDao itemDao) {
		super();
		this.listDao = listDao;
		this.itemDao = itemDao;
	}

	public GroceryListDao getListDao() {
		return listDao;
	}

	public void setListDao(GroceryListDao listDao) {
		this.listDao = listDao;
	}

	public GroceryItemDao getItemDao() {
		return itemDao;
	}

	public void setItemDao(GroceryItemDao itemDao) {
		this.itemDao = itemDao;
	}
}
