package com.example.controller;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.UserInfoDao;
import com.example.model.UserInfo;


@RestController
@Transactional
@CrossOrigin(origins = "*", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class UserInfoController {
   private UserInfoDao userDao;
   
   
   @GetMapping("/users")
	public List<UserInfo> getAllUsers() {
		return userDao.findAll();
	}
   
   @PostMapping("/user")
  	public UserInfo getUsers(@RequestBody UserInfo addUser) {
  		return userDao.findByUserNameAndPassword(addUser.getUserName(), addUser.getPassword());
  	}
   
   @PostMapping("/messageById")
 	public UserInfo getbyId(@RequestBody UserInfo User) {
 		return userDao.findByUserId(User.getUserId());
 	}
   
   @PostMapping("/users")
	public  UserInfo addUsers(@RequestBody UserInfo addUser) {
		return userDao.save(addUser);
	}
   
public UserInfoController(UserInfoDao userDao) {
	super();
	this.userDao = userDao;
}
public UserInfoDao getUserDao() {
	return userDao;
}
public void setUserDao(UserInfoDao userDao) {
	this.userDao = userDao;
}
   
   
}
