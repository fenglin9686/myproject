package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.UserInfo;

public interface UserInfoDao extends JpaRepository<UserInfo, Integer> {
	
  public UserInfo findByUserNameAndPassword(String username, String password);
  public UserInfo findByUserId(int id);
}
