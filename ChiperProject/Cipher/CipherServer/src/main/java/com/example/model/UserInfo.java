package com.example.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class UserInfo {

	
	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="user_password")
	private String password;
	
	@OneToMany(mappedBy="userId", fetch=FetchType.EAGER)
    private List<Ecypted> ecyptedList = new ArrayList<>();

	public UserInfo() {
	}
	
	public UserInfo(int userId, String userName, String password, List<Ecypted> ecyptedList) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.ecyptedList = ecyptedList;
	}

	public UserInfo(int userId) {
		super();
		this.userId = userId;
	}

	public UserInfo(String userName, String password, List<Ecypted> ecyptedList) {
		super();
		this.userName = userName;
		this.password = password;
		this.ecyptedList = ecyptedList;
	}

	public UserInfo(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Ecypted> getEcyptedList() {
		return ecyptedList;
	}

	public void setEcyptedList(List<Ecypted> ecyptedList) {
		this.ecyptedList = ecyptedList;
	}

	@Override
	public String toString() {
		return "UserInfo [userId=" + userId + ", userName=" + userName + ", password=" + password + ", ecyptedList="
				+ ecyptedList + "]";
	}
	
	
	
	
	
}
