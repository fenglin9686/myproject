package com.example.controller;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.EcyptedDao;
import com.example.model.Ecypted;



@RestController
@Transactional
@CrossOrigin(origins = "*", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class EcyptedController {
	private EcyptedDao ecyDao;

	@GetMapping("/allmessage")
	public List<Ecypted> allMessage(){
		return ecyDao.findAll();
	}
	@PostMapping("/savemessage/{key}")
	public Ecypted saveMessage( @RequestBody Ecypted message,  @PathVariable(name = "key") int key) {
		message.setMessage(Encrypted(key, message.getMessage()));
		return ecyDao.save(message);
	}
	
	@PostMapping("/getmessage/{key}")
	public String getMessage( @RequestBody Ecypted message,  @PathVariable(name = "key") int key) {
		return Decrypted(key, message.getMessage());
	}
	public String Decrypted(int key, String s) {
		String answer="";
        String alpha="abcdefghijklmnopqrstuvwxyz";
         String bigApha="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       for(int i=0; i<s.length(); i++){
           if(!Character.isLetter(s.charAt(i))){
               answer+=s.charAt(i);
           }
          else if(Character.isUpperCase(s.charAt(i))){
             int bigIdex=bigApha.indexOf(s.charAt(i))-key;
              if((bigIdex%26)<0){
                  answer+=bigApha.charAt((bigIdex%26+26));
              }
              else{
                answer+=bigApha.charAt(bigIdex%26);
              }
          }
          else{
              int bigIdex=alpha.indexOf(s.charAt(i))-key;
              if((bigIdex%26)<0){
                  answer+=alpha.charAt((bigIdex%26+26));
              }
              else{
                answer+=alpha.charAt(bigIdex%26);
              }
          }
        }
       return answer;
	}
	
	
	public String Encrypted(int key, String s) {
		String answer="";
        String alpha="abcdefghijklmnopqrstuvwxyz";
         String bigApha="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       for(int i=0; i<s.length(); i++){
           if(!Character.isLetter(s.charAt(i))){
               answer+=s.charAt(i);
           }
          else if(Character.isUpperCase(s.charAt(i))){
             int bigIdex=bigApha.indexOf(s.charAt(i))+key;
              if(bigIdex>25){
                  answer+=bigApha.charAt((bigIdex%26));
              }
              else{
                answer+=bigApha.charAt(bigIdex);
              }
          }
          else{
              int bigIdex=alpha.indexOf(s.charAt(i))+key;
              if(bigIdex>25){
                  answer+=alpha.charAt((bigIdex%26));
              }
              else{
                answer+=alpha.charAt(bigIdex);
              }
          }
        }
       return answer;
	}
	
	public EcyptedController(EcyptedDao ecyDao) {
		super();
		this.ecyDao = ecyDao;
	}

	public EcyptedDao getEcyDao() {
		return ecyDao;
	}

	public void setEcyDao(EcyptedDao ecyDao) {
		this.ecyDao = ecyDao;
	}
	
	
	
	
}
