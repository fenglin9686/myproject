package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="crypted")
public class Ecypted {

	@Id
	@Column(name="crypted_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int groceryid;
	 
	@Column(name="crypted_message")
	private String message;
	
	@Column(name="user_id")
	private int userId;

	public Ecypted() {
	}
	
	
	public Ecypted(int groceryid, String message, int userId) {
		super();
		this.groceryid = groceryid;
		this.message = message;
		this.userId = userId;
	}
	public Ecypted(String message, int userId) {
		super();
		this.message = message;
		this.userId = userId;
	}

	public int getGroceryid() {
		return groceryid;
	}

	public void setGroceryid(int groceryid) {
		this.groceryid = groceryid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Ecypted [groceryid=" + groceryid + ", message=" + message + ", userId=" + userId + "]";
	}
	
	
	
	
}
