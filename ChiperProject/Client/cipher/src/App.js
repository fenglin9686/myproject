import React from 'react';
import LoginRoute from "./Router/login"
import HomeRoute from "./Router/Main"
import './App.css';
import { BrowserRouter, Route} from 'react-router-dom'

function App() {
  return (
    <React.Fragment>
    <BrowserRouter>
        <Route exact path='/' component={LoginRoute}></Route>
        <Route path='/home' component={HomeRoute}></Route>
    </BrowserRouter>
</React.Fragment>

  );
}

export default App;
