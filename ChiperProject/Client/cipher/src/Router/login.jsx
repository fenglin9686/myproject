import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';


class login extends React.Component{

    constructor(props) {
        super(props)
        this.state={
            username:"",
            password:""
        }
        this.handleUserName = this.handleUserName.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    handleUserName(event){
        this.setState({
            username:event.target.value
        })
    }

    handlePassword(event){
        this.setState({
            password:event.target.value
        })
     }
     async onSubmit(event){
         event.preventDefault()
         const responsePayload = await axios({
            method: 'post',
            url: 'http://localhost:9025/user',
            data: {
                "userName": this.state.username,
                "password": this.state.password
            },
            withCredentials: true,
        });
        console.log(responsePayload.data)
         if(responsePayload.data){
              this.props.setUser(responsePayload.data.userId);
              this.props.history.push('/home');
          }
        
     }
    render() {
        return (
            <div>
            <form>
  <div class="form-group">
    <label for="exampleInputEmail1" >User Name</label>
    <input type="email" class="form-control"  onChange={this.handleUserName} placeholder="Enter User" />
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" placeholder="Password" onChange={this.handlePassword}/>
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1" />
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button onClick={this.onSubmit} type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
 )}

}
const mapDispatchToProps = dispatch => {
    return {
        setUser: (id) => dispatch({ type: 'LOGIN', id: id }),
    }
  }

export default connect(null, mapDispatchToProps)(withRouter(login))