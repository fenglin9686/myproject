import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';


class Main extends React.Component{

    constructor(props) {
        super(props);
         if (!this.props.current_id) {
           this.props.history.push("/");
         }
        this.state={
            allMessageList:[],
            userMessageList:[],
            eMessage:"",
            eKey:"",
            dKey:"",
            show:false
        }
        this.getAllMessageList=this.getAllMessageList.bind(this)
        this.getUserMessageList=this.getUserMessageList.bind(this)
        this.handlEemessage = this.handlEemessage.bind(this);
        this.handleEkey = this.handleEkey.bind(this);
        this.handleDkey = this.handleDkey.bind(this);
        this.onEncrypted=this.onEncrypted.bind(this);
        this.onDecrypted=this.onDecrypted.bind(this);
    }

    handleDkey(event){
        this.setState({
            dKey:event.target.value
        })
    }
    async onDecrypted(value){
        const test=await axios({
            method: 'post',
            url: 'http://localhost:9025/getmessage/'+this.state.dKey,
            data:{
                "message": value,
                "userId":this.props.current_id  
            },
            withCredentials: true,
        });
        window.alert(test.data);
    }
    async onEncrypted(event){

       await axios({
            method: 'post',
            url: 'http://localhost:9025/savemessage/'+this.state.eKey,
            data:{
                "message": this.state.eMessage,
                "userId": this.props.current_id
            },
            withCredentials: true,
        });
        this.setState({
        userMessageList:await this.getUserMessageList()
    })
    }
    handlEemessage(event){
        this.setState({
            eMessage:event.target.value
        })
    }
    handleEkey(event){
        this.setState({
            eKey:event.target.value
        })
       
    }
    async componentDidMount(){
        this.setState({
            allMessageList:await this.getAllMessageList(),
            userMessageList:await this.getUserMessageList(),
            show:true
        })
    }

    async getUserMessageList(){
        const payload=await axios({
            method: 'post',
            url: 'http://localhost:9025/messageById',
            data:{
                "userId":this.props.current_id
            },
            withCredentials: true,
        });
        return payload.data.ecyptedList;
    }
    async getAllMessageList(){
        const payload=await axios({
            method: 'get',
            url: 'http://localhost:9025/allmessage',
            withCredentials: true,
        });
        return payload.data;
    }



    render() {
        if(!this.state.show){
            return(
                <div>loading</div>
            )
        }
        else{
        return (
           <div>
               <h2>Use Table</h2>

               <div class="input-group">
        <div class="input-group-prepend">
        <span class="input-group-text" >Message</span>
        </div>
                <input type="text" class="form-control" onChange={this.handlEemessage}/>
         <span class="input-group-text">Key</span>
                <input type="text" onChange={this.handleEkey}/>
        <button class="btn btn-primary" onClick={this.onEncrypted}>Encrypted</button>
            </div>

      <br/>
        <span >Decrypted Key</span>
                <input type="text" onChange={this.handleDkey}/>
    


               <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Message Id</th>
                    <th scope="col">User Message</th>
                    <th scope="col">Button</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.userMessageList && 
                this.state.userMessageList.map((element, index) =>
                    <tr key={index}>
                    <th scope="row">{element.groceryid}</th>
                    <td>{element.message}</td>
                    <td> <button class="btn btn-primary"onClick={()=>this.onDecrypted(element.message)} >Encrypted</button></td>
                    </tr>
                    
                )} 
                </tbody>
                </table>

               <h3>Other User Message</h3>
               <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Message Id</th>
                    <th scope="col">User Message</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.allMessageList.map((element, index) =>
                    <tr key={index}>
                    <th scope="row">{element.groceryid}</th>
                    <td>{element.message}</td>
                    </tr>
                    
                )} 
                </tbody>
                </table>
           </div>
         )
        }
        }

}
const mapStateToProps = state => {
    return {
        current_id: state.userId,
    }
}

export default connect(mapStateToProps, null)(withRouter(Main));