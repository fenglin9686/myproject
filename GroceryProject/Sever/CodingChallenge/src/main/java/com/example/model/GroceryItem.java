package com.example.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Item")
public class GroceryItem {
	@Id
	@Column(name="item_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int itemid;
	
	@Column(name="item_name", nullable= false)
	private String itemname;
	
	@Column(name="item_type", nullable= false)
	private String itemtype;
	
	@Column(name="grocery_id")
	private int myGrocery;

	
	public GroceryItem() {
	}
	public GroceryItem(int itemid, String itemname, String itemtype, int myGrocery) {
		super();
		this.itemid = itemid;
		this.itemname = itemname;
		this.itemtype = itemtype;
		this.myGrocery = myGrocery;
	}
	public GroceryItem(String itemname, String itemtype, int myGrocery) {
		super();
		this.itemname = itemname;
		this.itemtype = itemtype;
		this.myGrocery = myGrocery;
	}

	public int getItemid() {
		return itemid;
	}

	public void setItemid(int itemid) {
		this.itemid = itemid;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getItemtype() {
		return itemtype;
	}

	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}

	public int getMyGrocery() {
		return myGrocery;
	}

	public void setMyGrocery(int myGrocery) {
		this.myGrocery = myGrocery;
	}
	@Override
	public String toString() {
		return "GroceryItem [itemid=" + itemid + ", itemname=" + itemname + ", itemtype=" + itemtype + ", myGrocery="
				+ myGrocery+ "]";
	}

		
	
}
