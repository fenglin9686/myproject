package com.example.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Grocery;

public interface GroceryDao extends CrudRepository<Grocery, Integer> {
		public List<Grocery> findAll();
		public Grocery findAllByGroceryid(int id);
		public Grocery save(Grocery newGrocery);
		public void deleteByGroceryid(int id);
}
