package com.example.controller;

import java.util.List;

import org.hibernate.cache.spi.support.AbstractReadWriteAccess.Item;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Grocery;
import com.example.repo.GroceryDao;
import com.example.repo.ItemDao;




@RestController
@Transactional
@CrossOrigin(origins = "*", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class GroceryController {	
private GroceryDao groceryDao;
private ItemDao itemDao;
	
	
	@GetMapping("/grocery-list")
	public List<Grocery> getGrocery() {
		return groceryDao.findAll();
	}

	@GetMapping("/grocery-list/{id}")
	public Grocery getGroceryItem(@PathVariable(name = "id") int id) {
		return groceryDao.findAllByGroceryid(id);
	}

	@PostMapping("/grocery-list")
	public Grocery postGrocery(@RequestBody Grocery newGrocery) {
		 return groceryDao.save(newGrocery);
	}
	
	@PutMapping("/grocery-list")
	public Grocery putGrocery(@RequestBody Grocery newGrocery) {
		 return groceryDao.save(newGrocery);
	}
	
	@DeleteMapping("/grocery-list/{id}")
	public void deleteGrocery(@PathVariable(name = "id") int id) {
		itemDao.deleteByMyGrocery(id);
		 groceryDao.deleteByGroceryid(id);
	}

	public GroceryController(GroceryDao groceryDao, ItemDao itemDao) {
		super();
		this.groceryDao = groceryDao;
		this.itemDao = itemDao;
	}

	public GroceryDao getGroceryDao() {
		return groceryDao;
	}

	public void setGroceryDao(GroceryDao groceryDao) {
		this.groceryDao = groceryDao;
	}

	public ItemDao getItemDao() {
		return itemDao;
	}

	public void setItemDao(ItemDao itemDao) {
		this.itemDao = itemDao;
	}
	
	
	

	
}
