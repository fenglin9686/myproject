package com.example.controller;

import java.util.List;

import org.hibernate.cache.spi.support.AbstractReadWriteAccess.Item;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.GroceryItem;
import com.example.repo.ItemDao;

@RestController
@Transactional
@CrossOrigin(origins = "*", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class ItemController {
	private ItemDao ItemDao;
	@GetMapping("/itemlist")
	public List<GroceryItem> getItem() {
		return ItemDao.findAll();
	}
	@PostMapping("/Item-list/{id}")
	public GroceryItem postItem(@PathVariable(name = "id") int id, @RequestBody GroceryItem newItem) {
		return ItemDao.save(newItem);
	}
	@PutMapping("/Item-list")
	public GroceryItem putGrocery(@RequestBody GroceryItem newItem) {
		 return ItemDao.save(newItem);
	}
	
	@DeleteMapping("/Item-list/{id}")
	public void deleteItem(@PathVariable(name = "id") int id) {
		ItemDao.deleteByItemid(id);
	}
	
	
	
	
	
	
	
	
	
	
	public ItemController(com.example.repo.ItemDao itemDao) {
		super();
		ItemDao = itemDao;
	}
	public ItemDao getItemDao() {
		return ItemDao;
	}
	public void setItemDao(ItemDao itemDao) {
		ItemDao = itemDao;
	}
	
	
	
}
