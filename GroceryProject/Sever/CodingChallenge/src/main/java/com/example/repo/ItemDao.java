package com.example.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.example.model.GroceryItem;

public interface ItemDao extends CrudRepository<GroceryItem, Integer> {
		public List<GroceryItem> findAll();
		public GroceryItem save(GroceryItem newItem);
		public void deleteByMyGrocery(int id);
		public void deleteByItemid(int id);
}
