const initialState = {
    groceryName:""
}
//Reducer
const reducer = (state = { ...initialState }, action) => {

    switch (action.type) {
        case "SET":
            console.log(action.name);
            return {
                groceryName: action.name
            }
        default:
            return {
                groceryName:"test"
            }
    }
}

export default reducer;
