import React from 'react';
import './App.css';
import axios from 'axios';
import { connect } from 'react-redux';
class Grocery extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
        list: null,
        wait:false,
        groceryid:"",
        groceryItemId:"",
        groceryname:"",
        insertGroceryname:"",
        insertItemType:"",
        insertItemName:"",
        itemid:"",
        itemtype:"",
        itemname:"",
        filterValue:"ALL",
        itemList:[]
    }
    this.handleGrocery = this.handleGrocery.bind(this);
    this.updateGrocery = this.updateGrocery.bind(this);
    this.handleGroceryUpdate = this.handleGroceryUpdate.bind(this);
    this.handleGroceryDelete = this.handleGroceryDelete.bind(this);
    this.handleInsertItemType = this.handleInsertItemType.bind(this);
    this.handleInsertItemName = this.handleInsertItemName.bind(this);
    this.insertItem=this.insertItem.bind(this);
   this.handleItemUpdate=this.handleItemUpdate.bind(this);
    this.handleItemDelete=this.handleItemDelete.bind(this);
    this.handleUpdateItemName=this.handleUpdateItemName.bind(this);
    this.handleUpdateItemType=this.handleUpdateItemType.bind(this);
    this.updateItem=this.updateItem.bind(this);
    this.handlefilter=this.handlefilter.bind(this);
    
}
/**
 *  For the filter Item by item type.
 * @param {*} event 
 */
handlefilter(event){
  this.setState({
   filterValue:event.target.value
  })
}
/**
 * Update the item to database, by item id, item name, item type and grocery id.
 */
async updateItem(){
  await axios({
    method: 'put',
    url: 'http://localhost:9025/Item-list',
    data: {
      "itemid": this.state.itemid,
      "itemname": this.state.itemname,
      "itemtype": this.state.itemtype,
      "myGrocery": this.state.groceryItemId
    },
    withCredentials: true,
});
this.handleGrocery(this.state.groceryItemId);
this.setState({
  itemid:"",
  itemname:"",
  itemtype:""
})
}

/**
 * update value when user select item type
 * @param {*} event 
 */
handleUpdateItemType(event){
  this.setState({
    itemtype: event.target.value
  })
}
/** 
 * When user  click update, fill item id, name and type to update field
 */
handleItemUpdate(id, name, type){
  this.setState({
    itemid:id,
    itemname:name,
    itemtype:type,
  })

}
async componentDidMount(){      
  this.setState({
    list: await this.getGroceryList(),
    wait:true
    })
}
/**
 * For delete item from databaseby id
 * @param {} id 
 */
async handleItemDelete(id){
   await axios({
    method: 'delete',
    url: 'http://localhost:9025/Item-list/'+id,
    withCredentials: true,
    });
    this.handleGrocery(this.state.groceryItemId);
}
/**
 * insert the item to data
 */
async insertItem(){
  await axios({
    method: 'post',
    url: 'http://localhost:9025/Item-list/1',
    data: {
      "itemname": this.state.insertItemName,
      "itemtype": this.state.insertItemType,
       "myGrocery": this.state.groceryItemId
    },
    withCredentials: true,
});
this.handleGrocery(this.state.groceryItemId);
  
}
async handleGroceryDelete(id){
   await axios({
    method: 'delete',
    url: 'http://localhost:9025/grocery-list/'+id,
    withCredentials: true,
    });
    this.setState({
      list: await this.getGroceryList(),
     })
    if(id==this.state.groceryItemId){
      this.setState({
        groceryItemId:"",
        itemList:[]
      })
    }
}
async getGroceryList(){
  let test= await axios({
      method: 'get',
      url: 'http://localhost:9025/grocery-list',
      withCredentials: true,
  });
  return test.data  
}

async updateGrocery(){
  const responsePayload = await axios({
    method: 'post',
    url: 'http://localhost:9025/grocery-list',
    data: {
      "groceryid":this.state.groceryid, 
      "groceryname": this.state.groceryname
    },
    withCredentials: true,
});
  console.log(responsePayload);
  this.setState({
    list: await this.getGroceryList(),
    })

}

async insertGrocery(){
  const responsePayload = await axios({
    method: 'put',
    url: 'http://localhost:9025/grocery-list',
    data: {
      "groceryname": this.props.getGrocery
    },
    withCredentials: true,
});
  console.log(responsePayload);
  this.setState({
    list: await this.getGroceryList(),
    })
}
handleInsertItemName(event){
  this.setState({
    insertItemName: event.target.value 
  })
}
handleUpdateItemName(event){
  this.setState({
    itemname: event.target.value 
  })
}
async handleGrocery(id){
  let test= await axios({
    method: 'get',
    url: 'http://localhost:9025/grocery-list/'+id,
    withCredentials: true,
});
this.setState({
  itemList:test.data.groceryList,
  groceryItemId: id
})
}
handleInsertItemType(event) {
  this.setState({insertItemType: event.target.value});
}
handleGroceryUpdate(id, name){
  this.setState({
      groceryid:id,
     groceryname:name
  })
}
updateGroceryName(event){
  this.setState({
    groceryname: event.target.value 
  })
}
UpdateinsertGroceryname(event){
  this.props.setGrocery(event.target.value );

}


  render() {
  if(!this.state.wait){
    return(
      <div>
         <p>Loading</p>
      </div>
      )
  }
    return(
    <div>
      <div className="groceryStyle">
       <input type="text" className="insertGrceryField" onChange={(value) => this.UpdateinsertGroceryname(value)} value={this.props.getGrocery}/>
       <button onClick={() => this.insertGrocery()}>Insert</button>
      <table>
      <tbody>
          <tr>
          <th>Grocery id</th>
          <th>Grocery name</th> 
          <th>Update</th>
          <th>Delete</th>
          </tr>
          {this.state.list.map((element, index) =>
            <tr key={index}>
              <td>{element.groceryid}</td>
              <td>{element.groceryname}</td>
            <td><button onClick={() => this.handleGroceryUpdate(element.groceryid, element.groceryname)}>update</button></td>
            <td><button onClick={() => this.handleGroceryDelete(element.groceryid)}>Delete</button></td>
            </tr>
          )}
          </tbody>
         </table>
   <div className="insertGrceryField">
    <label className="col-sm-2 col-form-label">id</label>
    <input type="text" readOnly className="form-control-plaintext" id="staticEmail" value={this.state.groceryid}/>
    <label  className="col-sm-2 col-form-label">Grocery</label>
      <input type="text" className="form-control" id="inputPassword" onChange={(value) => this.updateGroceryName(value)} value={this.state.groceryname}/>
  <button onClick={() => this.updateGrocery()}>Update grocery</button>
  </div>
  </div>
<br></br>
<br></br>

       {this.state.list.map((element, index) =>
      <button key={index} onClick={() => this.handleGrocery(element.groceryid)} > {element.groceryname}</button>)}
      {this.state.groceryItemId &&
      <div>
       <div class="input-group mb-3">
       <div class="input-group-prepend">
         <span class="input-group-text" id="inputGroup-sizing-default">Item name</span>
       </div>
       <input type="text" class="form-control" onChange={(value) => this.handleInsertItemName(value)} value={this.state.insertItemName}/>
     </div>
      <label for="exampleFormControlSelect1">Item Type</label>
      <select onChange={this.handleInsertItemType}>
            <option value=""></option>
            <option value="Food">Food</option>
            <option value="Electronics">Electronics</option>
            <option value="Cloth">Cloth</option>
          </select>
          <button onClick={() => this.insertItem()}>Insert new Item</button>

      </div>
     
      }
      {this.state.itemList.length>0 &&    
      <select value="ALL" value={this.state.filterValue} onChange={this.handlefilter}>
          <option value="ALL"></option>
            <option value="FOOD">Food</option>
            <option value="ELECTRONICS">Electronics</option>
            <option value="CLOTH">Cloth</option>
          </select>
          }
         <table>
         <tbody>
          <tr>
          <th>Item id</th>
          <th>Item name</th> 
          <th>Item Type</th> 
          <th>Update</th>
          <th>Delete</th>
          </tr>
          
          {this.state.itemList.length>0 &&
            this.state.itemList.map((element, index) =>
              <tr key={index}>
                <td>{element.itemid}</td>
                <td>{element.itemname}</td>
                <td>{element.itemtype}</td>
              <td><button onClick={() => this.handleItemUpdate(element.itemid, element.itemname, element.itemtype)}>update</button></td>
              <td><button onClick={() => this.handleItemDelete(element.itemid)}>Delete</button></td>
              </tr>    
            )
          }
           </tbody>
         </table>
         {this.state.itemList.length>0 &&
         <div>
            <span class="input-group-text" id="inputGroup-sizing-default">Item Id</span>
           <input type="text"  readOnly class="form-control" onChange={(value) => this.handleInsertItemName(value)} value={this.state.itemid}/>
         <span class="input-group-text" id="inputGroup-sizing-default">Item name</span>
        
       <input type="text" class="form-control" onChange={(value) => this.handleUpdateItemName(value)} value={this.state.itemname}/>
      <label for="exampleFormControlSelect1">Item Type</label>
      <select value={this.state.itemtype} onChange={this.handleUpdateItemType}>
            <option value=""></option>
            <option value="Food">Food</option>
            <option value="Electronics">Electronics</option>
            <option value="Cloth">Cloth</option>
          </select>
          <button onClick={() => this.updateItem()}>Update Item</button>
          </div>
  }
       
    </div>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return {
      setGrocery: (grocery) => dispatch({ type: 'SET', name: grocery }),
  }
}

const mapStateToProps = state => {
 // console.log(state.groceryName)
  return {
      getGrocery: state.groceryName,
  }
}; 


export default connect(mapStateToProps, mapDispatchToProps)(Grocery);
